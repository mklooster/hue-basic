class TimedExecutor{

	private long executeOnOrAfter;
	private Runnable runnable;

	public TimedExecutor(long viewTime, long executeInMs, Runnable runnable){
		executeOnOrAfter = viewTime + executeInMs;
		this.runnable = runnable;
	}

	public void execute(){
		runnable.run();
	}

	public boolean shouldExecute(long viewTime){
		return viewTime > executeOnOrAfter;
	}
}