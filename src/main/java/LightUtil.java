import org.json.JSONException;

/**
 * Created by kloos on 11/27/2017.
 */
public class LightUtil {

	public static boolean detectManualChange(Group group1, Group group2){
		LightState state1 = group1.getAction();
		LightState state2 = group2.getAction();
		return detectManualChange(state1, state2);
	}

	public static boolean detectManualChange(LightState state1, LightState state2){

		if(state1 == null || state2 == null){
			System.out.println("Skipping manual change detection because something was null");
			return false;
		}

		if(state1 == state2){
			System.out.println("States were the same!!");
		}

		if(state1.getBri() != null && state2.getBri() != null) {
			if (Math.abs(state1.getBri() - state2.getBri()) > 3) {
				System.out.println("Manual Bri Change! bri1: " + state1.getBri() + " bri2: " + state2.getBri());
				return true;
			}
		}
		if(state1.getHue() != null && state2.getHue() != null) {
			if (Math.abs(state1.getHue() - state2.getHue()) > 3) {
				System.out.println("Manual Hue Change! hue1: " + state1.getHue() + " hue2: " + state2.getHue());
				return true;
			}
		}
		if(state1.getSat() != null && state2.getSat() != null) {
			if (Math.abs(state1.getSat() - state2.getSat()) > 3) {
				System.out.println("Manual Sat Change! sat1: " + state1.getSat() + " sat2: " + state2.getSat());
				return true;
			}
		}
		if(state1.getOn() != null && state2.getOn() != null) {
			if (!state1.getOn().equals(state2.getOn())) {
				System.out.println("Manual On Change");
				return true;
			}
		}

		return false;
	}

	public static void setBrightness(Group group, long brightness) throws InterruptedException {

		LightState newState = new LightState();
		newState.setBri(brightness);

		if(group.getAction().getOn() && brightness <= 1){
			newState.setOn(false);
		}else if(!group.getAction().getOn() && brightness > 1){
			newState.setOn(true);
		}

		System.out.println("Changing lightGroup: " + group.getGroupId() + " /groups/" + group.getGroupId() + Constants.getGson().toJson(newState, LightState.class));
		RestUtil.put("/groups/" + group.getGroupId() + "/action", Constants.getGson().toJson(newState, LightState.class));
		group.setAction(newState);
	}

	public static void processNoMotion(Group group) throws JSONException, InterruptedException {
		// Update lights to new brightness.
		// Don't update if any current brightness is higher than period brightness.

		LightPeriod period = LightPeriod.getCurrentLightPeriod();
		long targetBrightness = period.getOffBrightness();
		if(!group.getAction().getOn() || group.getAction().getBri() <= targetBrightness){
			// Current brightness is already lower - exit.
			System.out.println("Lights already dark enough - Not doing anything.");
			return;
		}

		setBrightness(group, period.getOffBrightness());
	}

	public static void processMotion(Group group) throws JSONException, InterruptedException {
		// Update lights to new brightness.
		// Don't update if any current brightness is higher than period brightness.

		LightPeriod period = LightPeriod.getCurrentLightPeriod();
		long targetBrightness = period.getMotionBrightness();
		if(group.getAction().getOn() && group.getAction().getBri() >= targetBrightness){
			// Current brightness is already higher - exit.
			System.out.println("Lights already bright enough - Not doing anything.");
			return;
		}

		setBrightness(group, period.getMotionBrightness());
	}

	/*
	 * TODO: This isn't right - Lights keep brightening more than they should and eventually go to 100%.
	 */
	public static void dimGroup(Group group) throws JSONException, InterruptedException {
		double currentBrightness = 0;
		if(group.getAction().getOn()){
			currentBrightness = new Double(group.getAction().getBri()) / new Double(254);
		}

		long targetBrightness = (long) (254 * (Math.pow(40, (currentBrightness - 0.95)) - 0.1));

		System.out.println("Dimming... Current: " + currentBrightness);
		System.out.println("Output: " + targetBrightness);

		LightUtil.setBrightness(group, targetBrightness);
	}

	// Brightening function y=0.95+\log_{40}\left(x+0.1\right)
	public static void brightenGroup(Group group) throws JSONException, InterruptedException {
		double currentBrightness = 0;
		System.out.println("Brightening... On: " + group.getAction().getOn());
		if(group.getAction().getOn()){
			System.out.println("Setting current brightness to: " + group.getAction().getBri() + " / 254");
			currentBrightness = new Double(group.getAction().getBri()) / new Double(254);
		}

		long targetBrightness = (long) (254 * (0.95 + (Math.log(currentBrightness + 0.1) / Math.log(40))));

		System.out.println("Brightening... Current: " + currentBrightness);
		System.out.println("Output: " + targetBrightness);

		LightUtil.setBrightness(group, targetBrightness);
	}
}
