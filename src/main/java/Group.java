import java.util.List;

/**
 * Created by kloos on 11/18/2017.

 {
	 "name": "Kitchen",
	 "lights": [
		 "13",
		 "14",
		 "7",
		 "8"
	 ],
	 "type": "MotionMonitoredRoom",
	 "state": {
		 "all_on": false,
		 "any_on": false
	 },
	 "recycle": false,
	 "class": "Kitchen",
	 "action": {
		 "on": false,
		 "bri": 101,
		 "hue": 41492,
		 "sat": 78,
		 "effect": "none",
		 "xy": [
			 0.312,
			 0.328
		 ],
		 "ct": 153,
		 "alert": "none",
		 "colormode": "xy"
	 }
 }




 */
public class Group {

	private String groupId;

	private LightState action;
	private String name, type;
	private List<String> lights;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<String> getLights() {
		return lights;
	}

	public void setLights(List<String> lights) {
		this.lights = lights;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public LightState getAction() {
		return action;
	}

	public void setAction(LightState action) {
		this.action = action;
	}
}
