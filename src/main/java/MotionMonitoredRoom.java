import org.json.JSONException;

import java.util.function.Function;

/*
 * Represents a combo of lasers and motion sensors and light a group.
 *
 * Adds an occupant if motion is detected 2s following the laser being broken.
 * Adds an occupant if motion is detected when there are zero current occupants.
 *
 * Removes an occupants if laser is broken and there is no motion in 2s.
 * Removes all occupants after 1 hour of no motion.
 *
 */
public class MotionMonitoredRoom extends Monitor{

	protected static final Long MANUAL_CHANGE_TIMEOUT = new Long(10 * 60 * 1000); // 10 Minutes.
	protected static final Long LASER_DETECTION_TIMEOUT = 4000L; // 6 Seconds.
	protected static final Long NO_MOTION_AUTO_OFF = new Long(2 * 60 * 60 * 1000); // 2 Hours.

	protected boolean hasMotion = false;
	protected boolean manualChangeLockout = false;
	protected int occupants = 0;
	protected MotionMonitoredRoom adjacentRoom = null;

	protected final LightGroupMonitor lightGroupMonitor;
	protected final Function<LightGroupMonitor, Group> getLightGroup;

	protected TimedExecutor laserTimedExecutor = null;
	protected TimedExecutor manualChangeTimedExecutor = null;
	protected TimedExecutor noMotionTimedExecutor = null;


	public MotionMonitoredRoom(LightGroupMonitor lightGroupMonitor, Function<LightGroupMonitor, Group> getLightGroup) throws JSONException {
		this.lightGroupMonitor = lightGroupMonitor;
		this.getLightGroup = getLightGroup;
	}

	public void motionDetected(long viewTime){
		hasMotion = true;
		noMotionTimedExecutor = null;
		if(occupants < 1 && laserTimedExecutor == null){
			addOccupant();
		}
	}

	public void noMotionDetected(long viewTime){
		hasMotion = false;
		noMotionTimedExecutor = new TimedExecutor(viewTime, NO_MOTION_AUTO_OFF, this::processNoMotionAutoOff);
	}

	public void laserBroken(long viewTime){
		// Don't override a previous timed executor.
		if(laserTimedExecutor != null){
			return;
		}

		if(occupants < 1){
			addOccupant();
		}else{
			// Set the timer to either remove or add an occupant.
			laserTimedExecutor = new TimedExecutor(viewTime, LASER_DETECTION_TIMEOUT, this::checkMotionForOccupancy);
		}
	}

	public void manualChange(long viewTime){
		this.addManualChangeLockout();
		manualChangeTimedExecutor = new TimedExecutor(viewTime, MANUAL_CHANGE_TIMEOUT, this::removeManualChangeLockout);
	}

	public void addManualChangeLockout(){
		manualChangeLockout = true;
	}

	public void removeManualChangeLockout(){
		manualChangeLockout = false;
	}

	public void tick(long viewTime) throws JSONException {
		if(laserTimedExecutor != null && laserTimedExecutor.shouldExecute(viewTime)){
			laserTimedExecutor.execute();
			laserTimedExecutor = null;
		}
		if(manualChangeTimedExecutor != null && manualChangeTimedExecutor.shouldExecute(viewTime)){
			manualChangeTimedExecutor.execute();
			manualChangeTimedExecutor = null;
		}
		if(noMotionTimedExecutor != null && noMotionTimedExecutor.shouldExecute(viewTime)){
			noMotionTimedExecutor.execute();
			noMotionTimedExecutor = null;
		}
	}

	protected void processNoMotionAutoOff(){
		// Ignores manual change lockout.
		this.occupants = 0;
		this.processOccupancy();
	}

	protected void processOccupancy(){

		// Should change this group probably.
		Group lightGroup = getLightGroup.apply(lightGroupMonitor);
		System.out.println("Processing occupancy for group: " + lightGroup.getName() + " and occupants: " + occupants);

		try {
			if(occupants > 0) {
				LightUtil.processMotion(lightGroup);
			}else{
				LightUtil.setBrightness(lightGroup, 0);
			}
		} catch (JSONException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public long getRefreshFrequency() {
		// Very small refresh frequency because all events are based on API.
		return 100;
	}

	@Override
	public void refresh() throws JSONException {
		// Do nothing.  All sensors are based on API events.
	}

	protected void checkMotionForOccupancy(){
		if(this.hasMotion){
			this.addOccupant();
		}else{
			this.removeOccupant();
		}
	}

	protected void addOccupant(){
		this.occupants++;
		if(!manualChangeLockout) {
			this.processOccupancy();
		}
		if(adjacentRoom != null){
			adjacentRoom.removeOccupant();
		}
	}

	protected void removeOccupant(){
		if(this.occupants > 0){
			this.occupants--;
			if(!manualChangeLockout) {
				this.processOccupancy();
			}
			if(adjacentRoom != null){
				adjacentRoom.addOccupant();
			}
		}
	}

	public MotionMonitoredRoom getAdjacentRoom() {
		return adjacentRoom;
	}

	public void setAdjacentRoom(MotionMonitoredRoom adjacentRoom) {
		this.adjacentRoom = adjacentRoom;
	}
}
