import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

/**
 * Created by kloos on 11/18/2017.
 */
public class Constants {

	//public static final String LOCATION = "http://192.168.0.100/api/";
	public static final String LOCATION = "http://scobe.hopto.org:10555/api/";
	//public static final String LOCATION = "http://47.196.166.214:10555/api/";

	public static final String USER_NAME = "Dc7w3eGN66O7Un8Cr8NaCArk9vGgR9a5I6EQj-M9";

	public static Gson getGson(){
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
		return gsonBuilder.create();
	}


}
