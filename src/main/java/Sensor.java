/**
 * Created by kloos on 11/18/2017.
 *
 {
	 "state": {
		 "presence": false,
		 "lastupdated": "2017-11-28T01:58:29"
	 },
	 "swupdate": {
		 "state": "notupdatable",
		 "lastinstall": null
	 },
	 "config": {
		 "on": true,
		 "battery": 100,
		 "reachable": true,
		 "alert": "none",
		 "ledindication": false,
		 "usertest": false,
		 "sensitivity": 2,
		 "sensitivitymax": 2,
		 "pending": []
	 },
	 "name": "Hallway sensor",
	 "type": "ZLLPresence",
	 "modelid": "SML001",
	 "manufacturername": "Philips",
	 "swversion": "6.1.0.18912",
	 "uniqueid": "00:17:88:01:02:12:01:d3-02-0406"
 }

 */

public class Sensor {

	private String name, type, modelid, manufacurername, swversion, uniqueid;

	private SensorState state;

	public boolean hasMotion(){
		return this.getState().isPresence();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModelid() {
		return modelid;
	}

	public void setModelid(String modelid) {
		this.modelid = modelid;
	}

	public String getManufacurername() {
		return manufacurername;
	}

	public void setManufacurername(String manufacurername) {
		this.manufacurername = manufacurername;
	}

	public String getSwversion() {
		return swversion;
	}

	public void setSwversion(String swversion) {
		this.swversion = swversion;
	}

	public String getUniqueid() {
		return uniqueid;
	}

	public void setUniqueid(String uniqueid) {
		this.uniqueid = uniqueid;
	}

	public SensorState getState() {
		return state;
	}

	public void setState(SensorState state) {
		this.state = state;
	}
}
