/**
 * Created by kloos on 11/18/2017.

 {
	 "state": {
		 "hue": 50000,
		 "on": true,
		 "effect": "none",
		 "alert": "none",
		 "bri": 200,
		 "sat": 200,
		 "ct": 500,
		 "xy": [0.5, 0.5],
		 "reachable": true,
		 "colormode": "hs"
	 },
	 "type": "Living Colors",
	 "name": "LC 1",
	 "modelid": "LC0015",
	 "swversion": "1.0.3"
 }



 */
public class Light {

	private String lightId;

	private LightState state;
	private String type, name, modelid, swversion;

	public LightState getState() {
		return state;
	}

	public void setState(LightState state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getModelid() {
		return modelid;
	}

	public void setModelid(String modelid) {
		this.modelid = modelid;
	}

	public String getSwversion() {
		return swversion;
	}

	public void setSwversion(String swversion) {
		this.swversion = swversion;
	}

	public String getLightId() {
		return lightId;
	}

	public void setLightId(String lightId) {
		this.lightId = lightId;
	}
}
