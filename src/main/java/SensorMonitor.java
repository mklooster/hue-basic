import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;

/**
 * Created by kloos on 11/27/2017.
 */
public class SensorMonitor extends Monitor {

	public static final long REFRESH_FREQUENCY = 1500;

	public static final String KITCHEN_SENSOR_NAME = "Kitchen Sensor";
	public static final String HALLWAY_SENSOR_NAME = "Hallway sensor";

	public static final int KITCHEN_POST_MOTION_TIMEOUT = 15 * 60 * 1000; // 10 minutes.
	//public static final int HALL_POST_MOTION_TIMEOUT = 60 * 1000; // 1 minute.
	public static final int GUEST_BATHROOM_POST_MOTION_TIMEOUT = 45 * 60 * 1000; // 45 minutes.

	private static final String KITCHEN_MOTION_EVENT = "kitchenMotion";
	private static final String KITCHEN_NO_MOTION_EVENT = "kitchenNoMotion";

	private static final String HALLWAY_MOTION_EVENT = "hallwayMotion";
	private static final String HALLWAY_NO_MOTION_EVENT = "hallwayNoMotion";

	private static final String GUEST_BATH_MOTION_EVENT = "guestBathMotion";
	private static final String GUEST_BATH_NO_MOTION_EVENT = "guestBathNoMotion";

	private static SensorMonitor thisInstance = null;

	private Sensor kitchenSensor = null;
	private Sensor hallSensor = null;

	private boolean isMotionStateKitchen = false;
	private long lastMotionKitchen = 0l;

//	private boolean isMotionStateHallway = false;
//	private long lastMotionHallway = 0l;

	private boolean isMotionStateGuestBath = false;
	private long lastMotionGuestBathroom = 0l;

	public static SensorMonitor getInstance() throws JSONException {
		if(thisInstance == null){
			thisInstance = new SensorMonitor();
		}
		return thisInstance;
	}

	private SensorMonitor() throws JSONException {
		// Refresh adds the sensor instances.
		this.refresh();
	}

	@Override
	public void tick(long viewTime) throws JSONException {
		// Super will update the sensors if needed.
		super.tick(viewTime);

		if(this.kitchenSensor.hasMotion()){
			if(!isMotionStateKitchen){
				this.fireEvent(KITCHEN_MOTION_EVENT);
			}
			isMotionStateKitchen = true;
			lastMotionKitchen = viewTime;
		}else if((lastMotionKitchen + KITCHEN_POST_MOTION_TIMEOUT) < viewTime){
			if(isMotionStateKitchen) {
				this.fireEvent(KITCHEN_NO_MOTION_EVENT);
			}
			isMotionStateKitchen = false;
		}

		// TODO: Repurpose the "hall sensor"
		if(this.hallSensor.hasMotion()){
			//processHallwayLaser(viewTime);
		}else{
			//processHallwayNoMotion(viewTime);
		}

		// Reset the guest bath if it's still on after 45 minutes.
		if(isMotionStateGuestBath && ((lastMotionGuestBathroom + GUEST_BATHROOM_POST_MOTION_TIMEOUT) < viewTime)){
			this.fireEvent(GUEST_BATH_NO_MOTION_EVENT);
			isMotionStateGuestBath = false;
			lastMotionGuestBathroom = viewTime;
		}
	}

	public void processGuestBathroomMotion(long viewTime){
		if(isMotionStateGuestBath){
			this.fireEvent(GUEST_BATH_NO_MOTION_EVENT);
		}else{
			this.fireEvent(GUEST_BATH_MOTION_EVENT);
		}
		isMotionStateGuestBath = !isMotionStateGuestBath;
		lastMotionGuestBathroom = viewTime;
	}

//	public void processHallwayNoMotion(long viewTime){
//		if((lastMotionHallway + HALL_POST_MOTION_TIMEOUT) < viewTime) {
//			if (isMotionStateHallway) {
//				this.fireEvent(HALLWAY_NO_MOTION_EVENT);
//			}
//			isMotionStateHallway = false;
//		}
//	}
//
//	public void processHallwayMotion(long viewTime){
//		if(!isMotionStateHallway){
//			this.fireEvent(HALLWAY_MOTION_EVENT);
//		}
//		isMotionStateHallway = true;
//		lastMotionHallway = viewTime;
//	}

	public boolean onKitchenMotion(Runnable listener){
		return addEventListener(KITCHEN_MOTION_EVENT, listener);
	}

	public boolean onHallwayMotion(Runnable listener){
		return addEventListener(HALLWAY_MOTION_EVENT, listener);
	}

	public boolean onGuestBathMotion(Runnable listener){
		return addEventListener(GUEST_BATH_MOTION_EVENT, listener);
	}

	public boolean onKitchenNoMotion(Runnable listener){
		return addEventListener(KITCHEN_NO_MOTION_EVENT, listener);
	}

	public boolean onHallwayNoMotion(Runnable listener){
		return addEventListener(HALLWAY_NO_MOTION_EVENT, listener);
	}

	public boolean onGuestBathNoMotion(Runnable listener){
		return addEventListener(GUEST_BATH_NO_MOTION_EVENT, listener);
	}

	@Override
	public long getRefreshFrequency() {
		return REFRESH_FREQUENCY;
	}

	@Override
	public void refresh() throws JSONException {
		// First get the list of sensors
		JSONObject sensorList = new JSONObject(RestUtil.get("/sensors"));

		Iterator<String> keyList = sensorList.keys();
		while(keyList.hasNext()){
			String key = keyList.next();
			Sensor sensor = Constants.getGson().fromJson(sensorList.getJSONObject(key).toString(), Sensor.class);
			if(sensor.getName().equals(KITCHEN_SENSOR_NAME)){
				this.kitchenSensor = sensor;
			}else if(sensor.getName().equals(HALLWAY_SENSOR_NAME)){
				this.hallSensor = sensor;
			}
		}
	}

}
