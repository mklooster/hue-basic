import org.json.JSONException;

import java.util.*;

/**
 * Created by kloos on 11/27/2017.
 */
public abstract class Monitor {

	private long lastUpdated = 0l;

	private Map<String, List<Runnable>> eventRegistry = new HashMap<String, List<Runnable>>();

	public void fireEvent(String eventName){
		if(eventRegistry.get(eventName) == null){
			return;
		}
		for(Runnable eventListener : eventRegistry.get(eventName)){
			eventListener.run();
		}
	}

	public boolean addEventListener(String eventName, Runnable listener){

		// Ensure there's at least an empty list.
		if(eventRegistry.get(eventName) == null){
			eventRegistry.put(eventName, new ArrayList<Runnable>());
		}

		if(eventRegistry.get(eventName).contains(listener)){
			// Event already added.
			return false;
		}else{
			eventRegistry.get(eventName).add(listener);
			return true;
		}

	}

	public boolean removeEventListener(String eventName, Runnable listener){
		// Ensure there's at least an empty list.
		if(eventRegistry.get(eventName) == null){
			eventRegistry.put(eventName, new ArrayList<Runnable>());
		}

		return eventRegistry.get(eventName).remove(listener);
	}

	public void tick() throws JSONException {
		this.tick(new Date().getTime());
	}

	public void tick(long viewTime) throws JSONException {
		if(!this.needsRefresh(viewTime)){
			return;
		}
		this.refresh();
		this.lastUpdated = viewTime;
	}

	public boolean needsRefresh(){
		return this.needsRefresh(new Date().getTime());
	}

	public boolean needsRefresh(long viewTime){
		return (lastUpdated + getRefreshFrequency()) < viewTime;
	}

	public abstract long getRefreshFrequency();

	public abstract void refresh() throws JSONException;
}
