import org.json.JSONException;

import java.util.function.Function;

public class LaserMonitoredRoom extends MotionMonitoredRoom {

	public static final Long NO_MOTION_AUTO_OFF = new Long(60 * 1000); // 60 seconds.

	public LaserMonitoredRoom(LightGroupMonitor lightGroupMonitor, Function<LightGroupMonitor, Group> getLightGroup) throws JSONException {
		super(lightGroupMonitor, getLightGroup);
	}

	@Override
	public void laserBroken(long viewTime){
		if(this.occupants < 1){
			this.addOccupant();
		}else{
			this.removeOccupant();
		}
		noMotionTimedExecutor = new TimedExecutor(viewTime, NO_MOTION_AUTO_OFF, this::processNoMotionAutoOff);
	}

	@Override
	protected void checkMotionForOccupancy(){
		if(this.occupants < 1){
			this.addOccupant();
		}else{
			this.removeOccupant();
		}
	}

	@Override
	protected void processOccupancy(){
		if(manualChangeLockout){
			return;
		}

		// Should change this group probably.
		Group lightGroup = getLightGroup.apply(lightGroupMonitor);
		System.out.println("Processing occupancy for group: " + lightGroup.getName() + " and occupants: " + occupants);
		try {
			if(occupants > 0) {
				LightUtil.processMotion(lightGroup);
			}else{
				LightUtil.processNoMotion(lightGroup);
			}
		} catch (JSONException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
