import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by kloos on 11/27/2017.
 */
public class LightGroupMonitor extends Monitor {

	public static final long REFRESH_FREQUENCY = 10000;

	public static final String KITCHEN_LIGHT_GROUP_NAME = "Kitchen";
	public static final String HALLWAY_LIGHT_GROUP_NAME = "Hallway";
	public static final String GUEST_BATHROOM_LIGHT_GROUP_NAME = "Guest Bathroom";
	public static final String BATHROOM_LIGHT_GROUP_NAME = "Bathroom";

	public static final String KITCHEN_CHANGE_EVENT = "kitchenChange";
	public static final String HALLWAY_CHANGE_EVENT = "hallwayChange";
	public static final String GUEST_BATHROOM_CHANGE_EVENT = "guestBathroomChange";
	public static final String BATHROOM_CHANGE_EVENT = "bathroomChange";

	private Group kitchenGroup = null;
	private Group hallwayGroup = null;
	private Group guestBathroomGroup = null;
	private Group bathroomGroup = null;

	private Group kitchenGroupPrevious = null;
	private Group hallwayGroupPrevious = null;
	private Group guestBathroomGroupPrevious = null;
	private Group bathroomGroupPrevious = null;

	private static LightGroupMonitor instance = null;

	public static LightGroupMonitor getInstance() throws JSONException {
		if(instance == null){
			instance = new LightGroupMonitor();
		}
		return instance;
	}

	private LightGroupMonitor() throws JSONException {
		// Refresh adds the light groups.
		this.refresh();
		this.refresh();
	}

	@Override
	public void tick(long viewTime) throws JSONException {

		boolean needsRefresh = this.needsRefresh();

		// Super tick will refresh
		super.tick(viewTime);

		if(needsRefresh) {
			if (LightUtil.detectManualChange(kitchenGroupPrevious, kitchenGroup)) {
				fireEvent(KITCHEN_CHANGE_EVENT);
			}
			if (LightUtil.detectManualChange(hallwayGroupPrevious, hallwayGroup)) {
				fireEvent(HALLWAY_CHANGE_EVENT);
			}
			if (LightUtil.detectManualChange(guestBathroomGroupPrevious, guestBathroomGroup)) {
				fireEvent(GUEST_BATHROOM_CHANGE_EVENT);
			}
			if (LightUtil.detectManualChange(bathroomGroupPrevious, bathroomGroup)) {
				fireEvent(BATHROOM_CHANGE_EVENT);
			}
		}
	}

	@Override
	public void refresh() throws JSONException {
		this.refresh(false);
	}

	public void refresh(boolean hardRefresh) throws JSONException {

		// First get the list of sensors
		JSONObject groupList = new JSONObject(RestUtil.get("/groups"));

		// Setup the previous values.
		kitchenGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(kitchenGroup, Group.class).toString(), Group.class);
		hallwayGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(hallwayGroup, Group.class).toString(), Group.class);
		guestBathroomGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(guestBathroomGroup, Group.class).toString(), Group.class);
		bathroomGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(bathroomGroup, Group.class).toString(), Group.class);

		Iterator<String> keyList = groupList.keys();
		while (keyList.hasNext()) {
			String key = keyList.next();
			Group group = Constants.getGson().fromJson(groupList.getJSONObject(key).toString(), Group.class);
			group.setGroupId(key);
			if (group.getName().equals(KITCHEN_LIGHT_GROUP_NAME)) {
				kitchenGroup = group;
			} else if (group.getName().equals(HALLWAY_LIGHT_GROUP_NAME)) {
				hallwayGroup = group;
			} else if (group.getName().equals(GUEST_BATHROOM_LIGHT_GROUP_NAME)) {
				guestBathroomGroup = group;
			} else if (group.getName().equals(BATHROOM_LIGHT_GROUP_NAME)) {
				bathroomGroup = group;
			}
		}

		if(hardRefresh){
			// Setup the previous values.
			kitchenGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(kitchenGroup, Group.class).toString(), Group.class);
			hallwayGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(hallwayGroup, Group.class).toString(), Group.class);
			guestBathroomGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(guestBathroomGroup, Group.class).toString(), Group.class);
			bathroomGroupPrevious = Constants.getGson().fromJson(Constants.getGson().toJson(guestBathroomGroup, Group.class).toString(), Group.class);
		}

	}

	@Override
	public long getRefreshFrequency() {
		return REFRESH_FREQUENCY;
	}

	public boolean onKitchenChange(Runnable listener){
		return addEventListener(KITCHEN_CHANGE_EVENT, listener);
	}

	public boolean onHallwayChange(Runnable listener){
		return addEventListener(HALLWAY_CHANGE_EVENT, listener);
	}

	public boolean onGuestBathroomChange(Runnable listener){
		return addEventListener(GUEST_BATHROOM_CHANGE_EVENT, listener);
	}

	public boolean onBathroomChange(Runnable listener){
		return addEventListener(BATHROOM_CHANGE_EVENT, listener);
	}

	public Group getKitchenGroup() {
		return kitchenGroup;
	}

	public Group getHallwayGroup() {
		return hallwayGroup;
	}

	public Group getGuestBathroomGroup() {
		return guestBathroomGroup;
	}

	public Group getBathroomGroup() {
		return bathroomGroup;
	}

}
