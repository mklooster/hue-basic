import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.xml.internal.bind.v2.runtime.property.Property;

import java.util.Date;

import static com.sun.xml.internal.ws.developer.JAXWSProperties.CONNECT_TIMEOUT;

/**
 * Created by kloos on 11/18/2017.
 */
public class RestUtil {

	public static <K> K get(String location, Class<K> type){
		return new GsonBuilder().create().fromJson(RestUtil.get(location), type);
	}

	public static String get(String location){
		long startTime = new Date().getTime();
		//System.out.print("Api Call: " + location + "... ");
		try {

			Client client = Client.create();

			WebResource webResource = client.resource(Constants.LOCATION + Constants.USER_NAME + location);

			ClientResponse response = webResource.accept("application/json")
					.get(ClientResponse.class);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			return response.getEntity(String.class);

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//System.out.println("done. (" + (new Date().getTime() - startTime) + "ms)");
		}
		return null;
	}

	public static <K> K post(String location, String input, Class<K> type){
		return new GsonBuilder().create().fromJson(RestUtil.post(location, input), type);
	}

	public static String post(String location, String input){

		//System.out.println("Api Call: " + location);
		try {

			Client client = Client.create();

			WebResource webResource = client.resource(Constants.LOCATION + Constants.USER_NAME + location);

			ClientResponse response = webResource.accept("application/json")
					.post(ClientResponse.class, input);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			return response.getEntity(String.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static <K> K put(String location, String input, Class<K> type){
		return new GsonBuilder().create().fromJson(RestUtil.post(location, input), type);
	}

	public static String put(String location, String input){

		//System.out.println("Api Call: " + location);
		try {

			Client client = Client.create();

			WebResource webResource = client.resource(Constants.LOCATION + Constants.USER_NAME + location);

			ClientResponse response = webResource.accept("application/json")
					.put(ClientResponse.class, input);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			return response.getEntity(String.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}


}
