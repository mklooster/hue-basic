import org.json.JSONException;

import java.util.Date;

/**
 * Created by kloos on 11/18/2017.
 *
 * Kelsey complains that the motion sensor changes the light selection that she just made when she's in the kitchen.
 *
 * Motion sensor will recall the last light setting when motion is detected and increase brightness according to time of day.
 * Motion sensor will not make any changes to the lights during continious movement (no inactivity for 10 minutes).
 *
 * Motion sensor will dim or turn off lights after idle time according to time of day inactivity time depends on time of day.
 *
 */
public class AppMain {

	public static boolean DEBUG = true;

	public static final int LOOP_FREQUENCY = 50;

	public static final long KITCHEN_MANUAL_CHANGE_TIMEOUT = 30 * 60 * 1000; // 10 minutes.

	public static SensorMonitor sensorMonitor = null;
	public static LightGroupMonitor lightGroupMonitor = null;
	public static MotionMonitoredRoom bathroomRoomMonitor = null;
	public static LaserMonitoredRoom hallwayRoomMonitor = null;
	public static LaserMonitoredRoom guestBathroomMonitor = null;

	public static long kitchenManualChangeTimeout = 0l;

	public static Thread mainThread = null;
	public static final Object LOOPER_LOCK = new Object();

	public static void main(String[] args){

		try {
			initialize();
		} catch (JSONException e) {
			e.printStackTrace();
		}

		while(true){
			try{
				synchronized(LOOPER_LOCK){
					mainLooper();
				}
				Thread.sleep(LOOP_FREQUENCY);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void initialize() throws JSONException {

		mainThread = Thread.currentThread();

		// Create the Monitors
		lightGroupMonitor = LightGroupMonitor.getInstance();
		sensorMonitor = SensorMonitor.getInstance();

		// Motion listeners.
		if(DEBUG){
			sensorMonitor.onKitchenMotion(() -> System.out.println("Kitchen Motion Trigger!"));
			sensorMonitor.onHallwayMotion(() -> System.out.println("Hallway Motion Trigger!"));
		}
		sensorMonitor.onKitchenMotion(AppMain::processKitchenMotion);

		// No Motion Listeners.
		if(DEBUG){
			sensorMonitor.onKitchenNoMotion(() -> System.out.println("Kitchen NO Motion Trigger!"));
			sensorMonitor.onHallwayNoMotion(() -> System.out.println("Hallway NO Motion Trigger!"));
			sensorMonitor.onGuestBathNoMotion(() -> System.out.println("Guest Bath NO Motion Trigger!"));
		}
		sensorMonitor.onKitchenNoMotion(AppMain::processKitchenNoMotion);

		Runnable hardRefresh = () -> {
			try {
				lightGroupMonitor.refresh(true);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		};
		sensorMonitor.onKitchenMotion(hardRefresh);
		sensorMonitor.onKitchenNoMotion(hardRefresh);
		sensorMonitor.onGuestBathMotion(hardRefresh);
		sensorMonitor.onGuestBathNoMotion(hardRefresh);

		// Setup the bathroom/hallway and their fancy new API sensors.
		bathroomRoomMonitor = new MotionMonitoredRoom(lightGroupMonitor, LightGroupMonitor::getBathroomGroup);
		hallwayRoomMonitor = new LaserMonitoredRoom(lightGroupMonitor, LightGroupMonitor::getHallwayGroup);
		guestBathroomMonitor = new LaserMonitoredRoom(lightGroupMonitor, LightGroupMonitor::getGuestBathroomGroup);
		guestBathroomMonitor.setAdjacentRoom(hallwayRoomMonitor);


		// Manual change timeouts.
		if(DEBUG){
			lightGroupMonitor.onKitchenChange(() -> System.out.println("Kitchen Manual Change Trigger!"));
			lightGroupMonitor.onHallwayChange(() -> System.out.println("Hallway Manual Change Trigger!"));
			lightGroupMonitor.onGuestBathroomChange(() -> System.out.println("Guest Bathroom Manual Change Trigger!"));
		}
		lightGroupMonitor.onKitchenChange(() -> kitchenManualChangeTimeout = (new Date().getTime() + KITCHEN_MANUAL_CHANGE_TIMEOUT));
		lightGroupMonitor.onGuestBathroomChange(() -> guestBathroomMonitor.manualChange(new Date().getTime()));
		lightGroupMonitor.onHallwayChange(() -> hallwayRoomMonitor.manualChange(new Date().getTime()));

		// Start the API
		new SparkRoutes().init();
	}

	public static void mainLooper() throws JSONException {
		long tickTime = new Date().getTime();
		sensorMonitor.tick(tickTime);
		lightGroupMonitor.tick(tickTime);
		bathroomRoomMonitor.tick(tickTime);
		hallwayRoomMonitor.tick(tickTime);
	}

	public static void processKitchenMotion(){
		if(kitchenManualChangeTimeout > new Date().getTime()){
			// Manual change trumps.
			return;
		}
		try{
			LightUtil.processMotion(lightGroupMonitor.getKitchenGroup());
		} catch (InterruptedException | JSONException e) {
			e.printStackTrace();
		}
	}

	public static void processKitchenNoMotion(){
		if(kitchenManualChangeTimeout > new Date().getTime()){
			// Manual change trumps.
			return;
		}
		try{
			LightUtil.processNoMotion(lightGroupMonitor.getKitchenGroup());
		} catch (InterruptedException | JSONException e) {
			e.printStackTrace();
		}
	}

}
