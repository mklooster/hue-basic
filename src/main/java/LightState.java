/**
 * Created by kloos on 11/18/2017.

 {
	 "hue": 50000,
	 "on": true,
	 "effect": "none",
	 "alert": "none",
	 "bri": 200,
	 "sat": 200,
	 "ct": 500,
	 "xy": [0.5, 0.5],
	 "reachable": true,
	 "colormode": "hs"
 }

 */
public class LightState {

	private Long hue = null, bri = null, sat = null, ct = null;
	private String effect, alert, colormode;
	private Boolean on = null, reachable = null;
	private Double[] xy;

	public Long getHue() {
		return hue;
	}

	public void setHue(Long hue) {
		this.hue = hue;
	}

	public Long getBri() {
		return bri;
	}

	public void setBri(Long bri) {
		this.bri = bri;
	}

	public Long getSat() {
		return sat;
	}

	public void setSat(Long sat) {
		this.sat = sat;
	}

	public Long getCt() {
		return ct;
	}

	public void setCt(Long ct) {
		this.ct = ct;
	}

	public String getEffect() {
		return effect;
	}

	public void setEffect(String effect) {
		this.effect = effect;
	}

	public String getAlert() {
		return alert;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public String getColormode() {
		return colormode;
	}

	public void setColormode(String colormode) {
		this.colormode = colormode;
	}

	public Boolean getOn() {
		return on;
	}

	public void setOn(Boolean on) {
		this.on = on;
	}

	public Boolean getReachable() {
		return reachable;
	}

	public void setReachable(Boolean reachable) {
		this.reachable = reachable;
	}

	public Double[] getXy() {
		return xy;
	}

	public void setXy(Double[] xy) {
		this.xy = xy;
	}
}
