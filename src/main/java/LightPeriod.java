import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by kloos on 11/18/2017.
 */
public enum LightPeriod {

	SLEEP("23:00", "7:00", 0.3, 0),
	MORNING("7:00", "9:00", 0.9, 0),
	AFTERNOON("9:00", "18:00", 0.65, 0.2),
	NIGHT("18:00", "23:00", 0.75, 0.3);

	private int startMinute, endMinute;
	private double motionBrightness, offBrightness;

	LightPeriod(String startTime, String endTime, double motionBrightnessPercent, double offBrightnessPercent){

		// Break up by :
		String[] startTimeArr = startTime.split(":");
		String[] endTimeArr = endTime.split(":");

		int startHour = Integer.parseInt(startTimeArr[0]);
		int startMinuteTime = Integer.parseInt(startTimeArr[1]);

		int endHour = Integer.parseInt(endTimeArr[0]);
		int endMinuteTime = Integer.parseInt(endTimeArr[1]);

		this.startMinute = (startHour * 60) + startMinuteTime;
		this.endMinute = (endHour * 60) + endMinuteTime;

		System.out.println("registering period startTime: " + this.startMinute + " endTime: " + this.endMinute);

		this.motionBrightness = motionBrightnessPercent;
		this.offBrightness = offBrightnessPercent;

	}

	public long getMotionBrightness(){
		return (long) Math.floor(254 * this.motionBrightness);
	}

	public long getOffBrightness(){
		return (long) Math.floor(254 * this.offBrightness);
	}

	public static LightPeriod getCurrentLightPeriod(){

		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		int currentHour = cal.get(Calendar.HOUR_OF_DAY);
		int currentHourMinute = cal.get(Calendar.MINUTE);

		int currentMinute = (currentHour * 60) + currentHourMinute;

		for(LightPeriod per : LightPeriod.values()){
			if(per.startMinute <= currentMinute && per.endMinute >= currentMinute){
				return per;
			}
		}

		// Probably between first and last period.
		return LightPeriod.SLEEP;
	}


}
