import java.util.Date;

/**
 * Created by kloos on 11/18/2017.

 "state": {
	 "presence": false,
	 "lastupdated": "2017-11-28T01:58:29"
 },

 */
public class SensorState {

	public boolean presence;
	public Date lastupdated;

	public boolean isPresence() {
		return presence;
	}

	public void setPresence(boolean presence) {
		this.presence = presence;
	}

}
