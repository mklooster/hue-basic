import spark.Spark;

import java.util.Date;

/**
 * Created by kloos on 12/29/2017.
 */
public class SparkRoutes {

	public void init() {

		System.out.println("Starting spark routes...");

		// Route to undo all the manual change timeouts.
		Spark.get("/resetManualChange", (request, response) -> {
			synchronized (AppMain.LOOPER_LOCK){
				System.out.println("Manual Change Reset API");
				AppMain.kitchenManualChangeTimeout = 0l;
				AppMain.guestBathroomMonitor.removeManualChangeLockout();
				AppMain.hallwayRoomMonitor.removeManualChangeLockout();
			}
			return "";
		});

		// Under Projector.
		Spark.get("/lasermodule1", (request, response) -> {
			synchronized (AppMain.LOOPER_LOCK) {
				System.out.println("/lasermodule1: value:" + request.queryParams("value") + " fired hallway motion eventt");
				AppMain.hallwayRoomMonitor.laserBroken(new Date().getTime());
				LightGroupMonitor.getInstance().refresh(true);
				return "lasermodule1 success";
			}
		});

		// Guest Bathroom Doorway
		Spark.get("/lasermodule2", (request, response) -> {
			synchronized (AppMain.LOOPER_LOCK) {
				System.out.println("/lasermodule2: value:" + request.queryParams("value"));
				// TODO. Refactor to room.
				AppMain.guestBathroomMonitor.laserBroken(new Date().getTime());
				LightGroupMonitor.getInstance().refresh(true);
				return "lasermodule2 success";
			}
		});

		// Main Bathroom Doorway
		Spark.get("/lasermodule3", (request, response) -> {
			synchronized (AppMain.LOOPER_LOCK) {
				System.out.println("/lasermodule3: value:" + request.queryParams("value"));
				AppMain.bathroomRoomMonitor.laserBroken(new Date().getTime());
				LightGroupMonitor.getInstance().refresh(true);
				return "lasermodule3 success";
			}
		});

		// Main Bathroom
		Spark.get("/pirModule1", (request, response) -> {
			synchronized (AppMain.LOOPER_LOCK) {
				System.out.println("/pirModule1: value:" + request.queryParams("value"));
				if(request.queryParams("value").equals("1")){
					AppMain.bathroomRoomMonitor.motionDetected(new Date().getTime());
				}else{
					AppMain.bathroomRoomMonitor.noMotionDetected(new Date().getTime());
				}
				LightGroupMonitor.getInstance().refresh(true);
				return "pirModule1 success";
			}
		});
		Spark.init();
		System.out.println("Spark routes stareted!");
	}


}
